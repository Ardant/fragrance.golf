<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/bootslate.css" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>?<?php echo rand(1,999999999999999); ?>" />
<link type="text/plain" rel="author" href="/humans.txt" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
<?php wp_head(); ?>
</head>
<body ontouchstart="" <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<nav id="menu" role="navigation">
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
</header>
<div id="container" class="container-fluid">