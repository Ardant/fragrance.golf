<?php /*Template name: Home*/get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<section class="entry-content row">

<div class="col-sm-5 home-day"></div>
<div class="col-sm-2 home-intro">
<p>Welcome to Day and Night by Darren&nbsp;Clarke, fine fragrances for the discerning&nbsp;gentleman.</p>
<p>Pre-order now to secure the first&nbsp;edition.</p>
</div>
<div class="col-sm-5 home-night"></div>
<div class="home-branding">
	<img src="<?php echo get_template_directory_uri()?>/img/dclogo.png" alt="Darren Clarke Logo" />
    <h1>DARREN CLARKE</h1>
    <h2>DAY<span>&amp;</span>NIGHT</h2>
    <p class="first-edition">FIRST EDITION</p>
</div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>