<?php /*Template name: Products*/get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<section class="entry-content row">
<section id="home-products">

<?php $args = array(
    'post_type' => 'product',
    'paged' => $paged,
    'posts_per_page' => -1,
	'orderby'=>"date",
	'order'=>"ASC"
);

 $loop = new WP_Query($args);
             while($loop->have_posts()) : $loop->the_post(); ?>
			<div class="home-cell col-xs-12 col-sm-6 col-md-4">    
				<a href="<?php the_permalink(); ?>">
					<?php 
						$images = get_field('gallery', false, false);
						$image_id = $images[0];
						set_post_thumbnail( $post->ID, $image_id ); // CHECK OVERHEADS HERE
						the_post_thumbnail('medium');
					?>
					<h2><?php the_title(); ?></h2>
				</a>
			</div>
		<?php endwhile;
		
		wp_reset_query();
		
		?>







</section>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>