<?php /* Template name: Home */get_header(); ?>

<section id="fullpage" role="main">
	<div id="home"  data-anchor="home" class="section">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <section class="entry-content row">
            <div class="col-xs-12 col-home">
                <img src="<?php echo get_template_directory_uri()?>/img/homepage2.png" alt=""/>
            </div>
            </section>
        </article>
        <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
        <?php endwhile; endif; ?>
    </div>
    <div id="products" data-anchor="products" class="section product-section">
	<div class="row">
			<?php $args = array(
        'post_type' => 'product',
        'paged' => $paged,
        'posts_per_page' => -1,
        'orderby'=>"date",
        'order'=>"ASC"
    );
    
     $loop = new WP_Query($args);
                 while($loop->have_posts()) : $loop->the_post(); ?>
                <div class="home-cell col-xs-12 col-sm-4">    
                    <a href="<?php the_permalink(); ?>">
                        <?php 
                            $images = get_field('gallery', false, false);
                            $image_id = $images[0];
                            //set_post_thumbnail( $post->ID, $image_id ); 
                            the_post_thumbnail('medium');
                        ?>
                        <h2><?php the_title(); ?></h2>
                    </a>
                </div>
            <?php endwhile;
            
            wp_reset_query();
            
            ?>
	</div>
    </div>
    <section id="about" data-anchor="about" class="container section about-section">
    <div class="col-sm-8">	
      <h1>Profile</h1>
      <table id="profile-table">
        <tr>
          <td>Country:</td>
          <td>Northern Ireland</td>
        </tr>
        <tr>
          <td>DoB:</td>
          <td>14 August 1968</td>
        </tr>
        <tr>
          <td>Height:</td>
          <td>6ft 2ins (1.88 metres)</td>
        </tr>
        <tr>
          <td>Interests:</td>
          <td>Fishing, cigars, fine wines, cars &amp; Liverpool FC</td>
        </tr>
        <tr>
          <td>Attachment:</td>
          <td>The Astbury</td>
        </tr>
        <tr>
          <td>Family:</td>
          <td>Wife, Alison. Sons, Tyrone and Conor</td>
        </tr>
        <tr>
          <td>Tours:</td>
          <td>European Tour</td>
        </tr>
      </table>
      <h2>More about Darren</h2>
      <p>One of the world's best known golfers, Darren Clarke has won many fans with his passionate play and dedicated approach to the game.</p>
      <p>Darren won The Open Championship in 2011 at Royal St George's and also captained Europe's Ryder Cup team at Hazeltine in 2016.</p>
      <p>A stalwart of The European Tour since 1991, the Ulsterman is no stranger to firsts.</p>
      <p>In the 1999 Smurfit European Open he became the first player on The European Tour to shoot 60 for a second time, having achieved it first in the 1992 European Monte Carlo Open.  In 2002 he became the first player to win the English Open three times and in 2003 became the first player outside Tiger Woods to capture more than one World Golf Championship title.  Three weeks after the untimely death of wife Heather, who finally lost her brave battle against cancer in August 2006, Darren was picked as one of the wild cards for The Ryder Cup at K Club and in an emotionally charged week produced one of his most memorable performances, winning all three of his matches.</p>
      <p>A dedicated worker for charity, he set up his own Darren Clarke Foundation, which not only helps further the development of junior golf in Ireland, but also now raises money for Breast Cancer Awareness.</p>
       <p>Darren has assured his place in history by earning a place in the renowned 'Who's Who' guide for 2008, and in 2005 he even appeared on an Irish postage stamp. A difficult 2007, where Darren juggled looking after his two sons with his golf regime, saw him slip down the rankings, but he began to find his form again in South Africa before the winter break.</p>
      <p>2008 was a great year for Darren with wins in the BMW Asian Open in China and the KLM Open in The Netherlands where his sons Tyrone and Conor were there to witness Dad's victory.</p>
      <p>In 2010 he beat a world class field in the JP McManus Invitational Pro-Am at Adare Manor in Ireland where he had a one shot victory over Luke Donald. He secured his spot in the 139th Open Championship at St Andrews by finishing 2nd in the Barclays Scottish Open. Darren finished 30th in the European Tour Race</p>
      <p>To Dubai assuring his place in the field for the 2011 Open Championship at Royal St Georges. He rounded off 2010 in great style with the announcement of his engagement to Alison Campbell.</p>
      <p>2011 began well for Darren with a victory in the Iberdrola Open in Mallorca but he enjoyed his finest hour in July when he claimed his maiden major championship. Darren won the 140th Open Championship at Royal St George's in Kent by three shots from Phil Mickelson and Dustin Johnson.</p>
      <p>In April 2012 Darren and Alison were married at Abaco in the Bahamas which marked a very happy new chapter for the Clarke family.</p>
      <p>In February 2015, Darren was named as Europe's Ryder Cup captain and dedicated the next 18 months to the role. Ultimately, Europe were beaten 17-11 by the United States in Hazeltine.</p>
     
     </div> 
    </section>
    <section id="contact" data-anchor="contact" class="section contact-section container">
    <div class="col-sm-8">	
    	<h1>Contact</h1>
    </div>    
    </section>
</section>



<script>
// Add smooth scrolling to all links
  jQuery("#menu a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      jQuery('html, body').animate({
        scrollTop: jQuery(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });</script>
<?php get_footer(); ?>