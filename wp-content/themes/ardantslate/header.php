<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/bootslate.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/jquery.fullPage.css">-->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>?<?php echo rand(1,999999999999999); ?>" />
<link type="text/plain" rel="author" href="/humans.txt" />
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
<?php wp_head(); ?>
</head>
<body ontouchstart="" <?php body_class(); ?>>
<div id="dc"></div>
<div id="wrapper" class="hfeed">
<div id="menubar">
<ul id="menu">
<li><a href="/#home"><img src="/wp-content/uploads/2017/09/logosquare.png" alt=""/> HOME</a></li>
<li><a href="/#products">PRODUCTS</a></li>
<li><a href="/#about">ABOUT</a></li>
<li><a href="/#contact">CONTACT</a></li>
</ul>
</div>
<div id="container" class="container-fluid">